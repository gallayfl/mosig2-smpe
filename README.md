# MoSIG2-SMPE

Lecture notes + homeworks for the Scientific Methodology and Experimental Evaluation course.

Link towards the Gitlab created atomatically for the MOOC: [link](https://app-learninglab.inria.fr/moocrr/gitlab/153e7f97892d768a6ae9e326b6ce7919/mooc-rr)

It contains the exercises given during the MOOC (including the Challenger exercise).

The solution for the challenger exercise is given in the file containing the study (in french) : `exo5_fr.Rmd`.
