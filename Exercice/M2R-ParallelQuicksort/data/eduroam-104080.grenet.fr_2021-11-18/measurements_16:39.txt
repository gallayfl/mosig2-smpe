Size: 100
Sequential quicksort took: 0.000010 sec.
Parallel quicksort took: 0.004974 sec.
Built-in quicksort took: 0.000022 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.004287 sec.
Built-in quicksort took: 0.000020 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.005475 sec.
Built-in quicksort took: 0.000023 sec.
Size: 100
Sequential quicksort took: 0.000010 sec.
Parallel quicksort took: 0.005782 sec.
Built-in quicksort took: 0.000024 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.005475 sec.
Built-in quicksort took: 0.000023 sec.
Size: 1000
Sequential quicksort took: 0.000117 sec.
Parallel quicksort took: 0.020026 sec.
Built-in quicksort took: 0.000131 sec.
Size: 1000
Sequential quicksort took: 0.000107 sec.
Parallel quicksort took: 0.018963 sec.
Built-in quicksort took: 0.000122 sec.
Size: 1000
Sequential quicksort took: 0.000117 sec.
Parallel quicksort took: 0.017811 sec.
Built-in quicksort took: 0.000108 sec.
Size: 1000
Sequential quicksort took: 0.000090 sec.
Parallel quicksort took: 0.015523 sec.
Built-in quicksort took: 0.000124 sec.
Size: 1000
Sequential quicksort took: 0.000098 sec.
Parallel quicksort took: 0.021494 sec.
Built-in quicksort took: 0.000129 sec.
Size: 10000
Sequential quicksort took: 0.001378 sec.
Parallel quicksort took: 0.025757 sec.
Built-in quicksort took: 0.001317 sec.
Size: 10000
Sequential quicksort took: 0.001166 sec.
Parallel quicksort took: 0.029015 sec.
Built-in quicksort took: 0.001306 sec.
Size: 10000
Sequential quicksort took: 0.001166 sec.
Parallel quicksort took: 0.026482 sec.
Built-in quicksort took: 0.001311 sec.
Size: 10000
Sequential quicksort took: 0.001223 sec.
Parallel quicksort took: 0.030727 sec.
Built-in quicksort took: 0.001303 sec.
Size: 10000
Sequential quicksort took: 0.001170 sec.
Parallel quicksort took: 0.028540 sec.
Built-in quicksort took: 0.001310 sec.
Size: 100000
Sequential quicksort took: 0.014422 sec.
Parallel quicksort took: 0.039065 sec.
Built-in quicksort took: 0.015937 sec.
Size: 100000
Sequential quicksort took: 0.014222 sec.
Parallel quicksort took: 0.038944 sec.
Built-in quicksort took: 0.016031 sec.
Size: 100000
Sequential quicksort took: 0.014213 sec.
Parallel quicksort took: 0.038519 sec.
Built-in quicksort took: 0.015946 sec.
Size: 100000
Sequential quicksort took: 0.014269 sec.
Parallel quicksort took: 0.037583 sec.
Built-in quicksort took: 0.015943 sec.
Size: 100000
Sequential quicksort took: 0.014344 sec.
Parallel quicksort took: 0.048321 sec.
Built-in quicksort took: 0.015871 sec.
Size: 200000
Sequential quicksort took: 0.030384 sec.
Parallel quicksort took: 0.035597 sec.
Built-in quicksort took: 0.033632 sec.
Size: 200000
Sequential quicksort took: 0.030295 sec.
Parallel quicksort took: 0.035961 sec.
Built-in quicksort took: 0.033737 sec.
Size: 200000
Sequential quicksort took: 0.030214 sec.
Parallel quicksort took: 0.038975 sec.
Built-in quicksort took: 0.033775 sec.
Size: 200000
Sequential quicksort took: 0.031301 sec.
Parallel quicksort took: 0.036724 sec.
Built-in quicksort took: 0.033718 sec.
Size: 200000
Sequential quicksort took: 0.030130 sec.
Parallel quicksort took: 0.035390 sec.
Built-in quicksort took: 0.033741 sec.
Size: 300000
Sequential quicksort took: 0.047192 sec.
Parallel quicksort took: 0.046902 sec.
Built-in quicksort took: 0.051587 sec.
Size: 300000
Sequential quicksort took: 0.047159 sec.
Parallel quicksort took: 0.043650 sec.
Built-in quicksort took: 0.050585 sec.
Size: 300000
Sequential quicksort took: 0.047328 sec.
Parallel quicksort took: 0.048386 sec.
Built-in quicksort took: 0.056612 sec.
Size: 300000
Sequential quicksort took: 0.047005 sec.
Parallel quicksort took: 0.046028 sec.
Built-in quicksort took: 0.051117 sec.
Size: 300000
Sequential quicksort took: 0.047468 sec.
Parallel quicksort took: 0.045293 sec.
Built-in quicksort took: 0.051468 sec.
Size: 400000
Sequential quicksort took: 0.067248 sec.
Parallel quicksort took: 0.049944 sec.
Built-in quicksort took: 0.071418 sec.
Size: 400000
Sequential quicksort took: 0.062077 sec.
Parallel quicksort took: 0.048404 sec.
Built-in quicksort took: 0.071350 sec.
Size: 400000
Sequential quicksort took: 0.063461 sec.
Parallel quicksort took: 0.047082 sec.
Built-in quicksort took: 0.070931 sec.
Size: 400000
Sequential quicksort took: 0.064548 sec.
Parallel quicksort took: 0.042544 sec.
Built-in quicksort took: 0.070862 sec.
Size: 400000
Sequential quicksort took: 0.063065 sec.
Parallel quicksort took: 0.037159 sec.
Built-in quicksort took: 0.070116 sec.
Size: 500000
Sequential quicksort took: 0.081492 sec.
Parallel quicksort took: 0.055695 sec.
Built-in quicksort took: 0.089988 sec.
Size: 500000
Sequential quicksort took: 0.079705 sec.
Parallel quicksort took: 0.051052 sec.
Built-in quicksort took: 0.090212 sec.
Size: 500000
Sequential quicksort took: 0.078275 sec.
Parallel quicksort took: 0.048701 sec.
Built-in quicksort took: 0.089907 sec.
Size: 500000
Sequential quicksort took: 0.079166 sec.
Parallel quicksort took: 0.052243 sec.
Built-in quicksort took: 0.089735 sec.
Size: 500000
Sequential quicksort took: 0.078654 sec.
Parallel quicksort took: 0.050775 sec.
Built-in quicksort took: 0.096247 sec.
Size: 600000
Sequential quicksort took: 0.099055 sec.
Parallel quicksort took: 0.049345 sec.
Built-in quicksort took: 0.110451 sec.
Size: 600000
Sequential quicksort took: 0.100229 sec.
Parallel quicksort took: 0.047771 sec.
Built-in quicksort took: 0.114921 sec.
Size: 600000
Sequential quicksort took: 0.101815 sec.
Parallel quicksort took: 0.048290 sec.
Built-in quicksort took: 0.118163 sec.
Size: 600000
Sequential quicksort took: 0.122414 sec.
Parallel quicksort took: 0.046260 sec.
Built-in quicksort took: 0.109572 sec.
Size: 600000
Sequential quicksort took: 0.100385 sec.
Parallel quicksort took: 0.051036 sec.
Built-in quicksort took: 0.109197 sec.
Size: 700000
Sequential quicksort took: 0.116648 sec.
Parallel quicksort took: 0.060551 sec.
Built-in quicksort took: 0.129099 sec.
Size: 700000
Sequential quicksort took: 0.114700 sec.
Parallel quicksort took: 0.056587 sec.
Built-in quicksort took: 0.129342 sec.
Size: 700000
Sequential quicksort took: 0.115234 sec.
Parallel quicksort took: 0.061730 sec.
Built-in quicksort took: 0.128118 sec.
Size: 700000
Sequential quicksort took: 0.113923 sec.
Parallel quicksort took: 0.056584 sec.
Built-in quicksort took: 0.128423 sec.
Size: 700000
Sequential quicksort took: 0.113150 sec.
Parallel quicksort took: 0.055637 sec.
Built-in quicksort took: 0.128714 sec.
Size: 800000
Sequential quicksort took: 0.137302 sec.
Parallel quicksort took: 0.050409 sec.
Built-in quicksort took: 0.148190 sec.
Size: 800000
Sequential quicksort took: 0.131464 sec.
Parallel quicksort took: 0.057677 sec.
Built-in quicksort took: 0.147557 sec.
Size: 800000
Sequential quicksort took: 0.132332 sec.
Parallel quicksort took: 0.055498 sec.
Built-in quicksort took: 0.171694 sec.
Size: 800000
Sequential quicksort took: 0.135371 sec.
Parallel quicksort took: 0.061554 sec.
Built-in quicksort took: 0.149765 sec.
Size: 800000
Sequential quicksort took: 0.131491 sec.
Parallel quicksort took: 0.053223 sec.
Built-in quicksort took: 0.148459 sec.
Size: 900000
Sequential quicksort took: 0.151041 sec.
Parallel quicksort took: 0.054875 sec.
Built-in quicksort took: 0.165963 sec.
Size: 900000
Sequential quicksort took: 0.150430 sec.
Parallel quicksort took: 0.061578 sec.
Built-in quicksort took: 0.166102 sec.
Size: 900000
Sequential quicksort took: 0.146855 sec.
Parallel quicksort took: 0.057392 sec.
Built-in quicksort took: 0.167048 sec.
Size: 900000
Sequential quicksort took: 0.147523 sec.
Parallel quicksort took: 0.060145 sec.
Built-in quicksort took: 0.165242 sec.
Size: 900000
Sequential quicksort took: 0.146684 sec.
Parallel quicksort took: 0.047192 sec.
Built-in quicksort took: 0.165293 sec.
Size: 1000000
Sequential quicksort took: 0.166026 sec.
Parallel quicksort took: 0.061950 sec.
Built-in quicksort took: 0.185997 sec.
Size: 1000000
Sequential quicksort took: 0.163615 sec.
Parallel quicksort took: 0.060720 sec.
Built-in quicksort took: 0.188459 sec.
Size: 1000000
Sequential quicksort took: 0.166679 sec.
Parallel quicksort took: 0.057213 sec.
Built-in quicksort took: 0.185117 sec.
Size: 1000000
Sequential quicksort took: 0.164563 sec.
Parallel quicksort took: 0.069605 sec.
Built-in quicksort took: 0.187483 sec.
Size: 1000000
Sequential quicksort took: 0.167351 sec.
Parallel quicksort took: 0.064664 sec.
Built-in quicksort took: 0.186953 sec.
