Size: 100
Sequential quicksort took: 0.000005 sec.
Parallel quicksort took: 0.004775 sec.
Built-in quicksort took: 0.000014 sec.
Size: 100
Sequential quicksort took: 0.000005 sec.
Parallel quicksort took: 0.003872 sec.
Built-in quicksort took: 0.000015 sec.
Size: 100
Sequential quicksort took: 0.000004 sec.
Parallel quicksort took: 0.004215 sec.
Built-in quicksort took: 0.000013 sec.
Size: 100
Sequential quicksort took: 0.000005 sec.
Parallel quicksort took: 0.003716 sec.
Built-in quicksort took: 0.000013 sec.
Size: 100
Sequential quicksort took: 0.000005 sec.
Parallel quicksort took: 0.002880 sec.
Built-in quicksort took: 0.000014 sec.
Size: 1000
Sequential quicksort took: 0.000055 sec.
Parallel quicksort took: 0.014572 sec.
Built-in quicksort took: 0.000090 sec.
Size: 1000
Sequential quicksort took: 0.000054 sec.
Parallel quicksort took: 0.013664 sec.
Built-in quicksort took: 0.000083 sec.
Size: 1000
Sequential quicksort took: 0.000048 sec.
Parallel quicksort took: 0.014351 sec.
Built-in quicksort took: 0.000082 sec.
Size: 1000
Sequential quicksort took: 0.000047 sec.
Parallel quicksort took: 0.012827 sec.
Built-in quicksort took: 0.000081 sec.
Size: 1000
Sequential quicksort took: 0.000047 sec.
Parallel quicksort took: 0.012390 sec.
Built-in quicksort took: 0.000080 sec.
Size: 10000
Sequential quicksort took: 0.000567 sec.
Parallel quicksort took: 0.033738 sec.
Built-in quicksort took: 0.000916 sec.
Size: 10000
Sequential quicksort took: 0.000569 sec.
Parallel quicksort took: 0.028934 sec.
Built-in quicksort took: 0.000916 sec.
Size: 10000
Sequential quicksort took: 0.000564 sec.
Parallel quicksort took: 0.027862 sec.
Built-in quicksort took: 0.000918 sec.
Size: 10000
Sequential quicksort took: 0.000567 sec.
Parallel quicksort took: 0.024432 sec.
Built-in quicksort took: 0.000913 sec.
Size: 10000
Sequential quicksort took: 0.000568 sec.
Parallel quicksort took: 0.034010 sec.
Built-in quicksort took: 0.000927 sec.
Size: 100000
Sequential quicksort took: 0.007222 sec.
Parallel quicksort took: 0.044323 sec.
Built-in quicksort took: 0.011052 sec.
Size: 100000
Sequential quicksort took: 0.006717 sec.
Parallel quicksort took: 0.034773 sec.
Built-in quicksort took: 0.011385 sec.
Size: 100000
Sequential quicksort took: 0.009016 sec.
Parallel quicksort took: 0.040942 sec.
Built-in quicksort took: 0.011094 sec.
Size: 100000
Sequential quicksort took: 0.006666 sec.
Parallel quicksort took: 0.034475 sec.
Built-in quicksort took: 0.011047 sec.
Size: 100000
Sequential quicksort took: 0.006710 sec.
Parallel quicksort took: 0.034902 sec.
Built-in quicksort took: 0.011047 sec.
Size: 200000
Sequential quicksort took: 0.014070 sec.
Parallel quicksort took: 0.039569 sec.
Built-in quicksort took: 0.025186 sec.
Size: 200000
Sequential quicksort took: 0.014284 sec.
Parallel quicksort took: 0.039144 sec.
Built-in quicksort took: 0.023231 sec.
Size: 200000
Sequential quicksort took: 0.014061 sec.
Parallel quicksort took: 0.039029 sec.
Built-in quicksort took: 0.023243 sec.
Size: 200000
Sequential quicksort took: 0.014088 sec.
Parallel quicksort took: 0.035214 sec.
Built-in quicksort took: 0.023203 sec.
Size: 200000
Sequential quicksort took: 0.014082 sec.
Parallel quicksort took: 0.035333 sec.
Built-in quicksort took: 0.023175 sec.
Size: 300000
Sequential quicksort took: 0.021779 sec.
Parallel quicksort took: 0.042043 sec.
Built-in quicksort took: 0.036090 sec.
Size: 300000
Sequential quicksort took: 0.021462 sec.
Parallel quicksort took: 0.043633 sec.
Built-in quicksort took: 0.036013 sec.
Size: 300000
Sequential quicksort took: 0.021444 sec.
Parallel quicksort took: 0.044610 sec.
Built-in quicksort took: 0.035795 sec.
Size: 300000
Sequential quicksort took: 0.021373 sec.
Parallel quicksort took: 0.041819 sec.
Built-in quicksort took: 0.035833 sec.
Size: 300000
Sequential quicksort took: 0.021343 sec.
Parallel quicksort took: 0.040940 sec.
Built-in quicksort took: 0.036589 sec.
Size: 400000
Sequential quicksort took: 0.033617 sec.
Parallel quicksort took: 0.043807 sec.
Built-in quicksort took: 0.049495 sec.
Size: 400000
Sequential quicksort took: 0.030649 sec.
Parallel quicksort took: 0.043424 sec.
Built-in quicksort took: 0.049124 sec.
Size: 400000
Sequential quicksort took: 0.029886 sec.
Parallel quicksort took: 0.043705 sec.
Built-in quicksort took: 0.049066 sec.
Size: 400000
Sequential quicksort took: 0.029818 sec.
Parallel quicksort took: 0.045120 sec.
Built-in quicksort took: 0.049073 sec.
Size: 400000
Sequential quicksort took: 0.029288 sec.
Parallel quicksort took: 0.046034 sec.
Built-in quicksort took: 0.048440 sec.
Size: 500000
Sequential quicksort took: 0.037568 sec.
Parallel quicksort took: 0.044474 sec.
Built-in quicksort took: 0.061888 sec.
Size: 500000
Sequential quicksort took: 0.037253 sec.
Parallel quicksort took: 0.043847 sec.
Built-in quicksort took: 0.061308 sec.
Size: 500000
Sequential quicksort took: 0.037504 sec.
Parallel quicksort took: 0.039189 sec.
Built-in quicksort took: 0.062670 sec.
Size: 500000
Sequential quicksort took: 0.042741 sec.
Parallel quicksort took: 0.043970 sec.
Built-in quicksort took: 0.061814 sec.
Size: 500000
Sequential quicksort took: 0.039389 sec.
Parallel quicksort took: 0.039035 sec.
Built-in quicksort took: 0.061864 sec.
Size: 600000
Sequential quicksort took: 0.045502 sec.
Parallel quicksort took: 0.042632 sec.
Built-in quicksort took: 0.075830 sec.
Size: 600000
Sequential quicksort took: 0.045572 sec.
Parallel quicksort took: 0.048413 sec.
Built-in quicksort took: 0.079757 sec.
Size: 600000
Sequential quicksort took: 0.045649 sec.
Parallel quicksort took: 0.052306 sec.
Built-in quicksort took: 0.075111 sec.
Size: 600000
Sequential quicksort took: 0.045723 sec.
Parallel quicksort took: 0.051610 sec.
Built-in quicksort took: 0.077119 sec.
Size: 600000
Sequential quicksort took: 0.045787 sec.
Parallel quicksort took: 0.050333 sec.
Built-in quicksort took: 0.075610 sec.
Size: 700000
Sequential quicksort took: 0.051616 sec.
Parallel quicksort took: 0.052039 sec.
Built-in quicksort took: 0.089263 sec.
Size: 700000
Sequential quicksort took: 0.052474 sec.
Parallel quicksort took: 0.049447 sec.
Built-in quicksort took: 0.090232 sec.
Size: 700000
Sequential quicksort took: 0.052329 sec.
Parallel quicksort took: 0.048494 sec.
Built-in quicksort took: 0.089421 sec.
Size: 700000
Sequential quicksort took: 0.053165 sec.
Parallel quicksort took: 0.048027 sec.
Built-in quicksort took: 0.092114 sec.
Size: 700000
Sequential quicksort took: 0.052566 sec.
Parallel quicksort took: 0.051928 sec.
Built-in quicksort took: 0.087990 sec.
Size: 800000
Sequential quicksort took: 0.061575 sec.
Parallel quicksort took: 0.051637 sec.
Built-in quicksort took: 0.102166 sec.
Size: 800000
Sequential quicksort took: 0.061711 sec.
Parallel quicksort took: 0.051486 sec.
Built-in quicksort took: 0.113638 sec.
Size: 800000
Sequential quicksort took: 0.061102 sec.
Parallel quicksort took: 0.048655 sec.
Built-in quicksort took: 0.102716 sec.
Size: 800000
Sequential quicksort took: 0.063621 sec.
Parallel quicksort took: 0.047702 sec.
Built-in quicksort took: 0.102304 sec.
Size: 800000
Sequential quicksort took: 0.061696 sec.
Parallel quicksort took: 0.047623 sec.
Built-in quicksort took: 0.102600 sec.
Size: 900000
Sequential quicksort took: 0.070790 sec.
Parallel quicksort took: 0.053609 sec.
Built-in quicksort took: 0.117934 sec.
Size: 900000
Sequential quicksort took: 0.069889 sec.
Parallel quicksort took: 0.052031 sec.
Built-in quicksort took: 0.117167 sec.
Size: 900000
Sequential quicksort took: 0.070330 sec.
Parallel quicksort took: 0.054926 sec.
Built-in quicksort took: 0.115701 sec.
Size: 900000
Sequential quicksort took: 0.069518 sec.
Parallel quicksort took: 0.052460 sec.
Built-in quicksort took: 0.116723 sec.
Size: 900000
Sequential quicksort took: 0.068203 sec.
Parallel quicksort took: 0.051250 sec.
Built-in quicksort took: 0.117763 sec.
Size: 1000000
Sequential quicksort took: 0.077924 sec.
Parallel quicksort took: 0.038069 sec.
Built-in quicksort took: 0.130950 sec.
Size: 1000000
Sequential quicksort took: 0.079165 sec.
Parallel quicksort took: 0.036752 sec.
Built-in quicksort took: 0.130437 sec.
Size: 1000000
Sequential quicksort took: 0.077470 sec.
Parallel quicksort took: 0.036300 sec.
Built-in quicksort took: 0.128378 sec.
Size: 1000000
Sequential quicksort took: 0.078200 sec.
Parallel quicksort took: 0.057012 sec.
Built-in quicksort took: 0.129246 sec.
Size: 1000000
Sequential quicksort took: 0.078459 sec.
Parallel quicksort took: 0.053307 sec.
Built-in quicksort took: 0.130985 sec.
