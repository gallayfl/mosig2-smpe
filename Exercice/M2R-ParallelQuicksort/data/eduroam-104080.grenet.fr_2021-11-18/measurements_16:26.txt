Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.004386 sec.
Built-in quicksort took: 0.000021 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.004895 sec.
Built-in quicksort took: 0.000023 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.004323 sec.
Built-in quicksort took: 0.000021 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.003319 sec.
Built-in quicksort took: 0.000021 sec.
Size: 100
Sequential quicksort took: 0.000009 sec.
Parallel quicksort took: 0.003607 sec.
Built-in quicksort took: 0.000016 sec.
Size: 1000
Sequential quicksort took: 0.000115 sec.
Parallel quicksort took: 0.015980 sec.
Built-in quicksort took: 0.000125 sec.
Size: 1000
Sequential quicksort took: 0.000109 sec.
Parallel quicksort took: 0.014248 sec.
Built-in quicksort took: 0.000108 sec.
Size: 1000
Sequential quicksort took: 0.000095 sec.
Parallel quicksort took: 0.011414 sec.
Built-in quicksort took: 0.000108 sec.
Size: 1000
Sequential quicksort took: 0.000094 sec.
Parallel quicksort took: 0.014619 sec.
Built-in quicksort took: 0.000109 sec.
Size: 1000
Sequential quicksort took: 0.000094 sec.
Parallel quicksort took: 0.011947 sec.
Built-in quicksort took: 0.000110 sec.
Size: 10000
Sequential quicksort took: 0.001178 sec.
Parallel quicksort took: 0.029578 sec.
Built-in quicksort took: 0.001284 sec.
Size: 10000
Sequential quicksort took: 0.001173 sec.
Parallel quicksort took: 0.023026 sec.
Built-in quicksort took: 0.001290 sec.
Size: 10000
Sequential quicksort took: 0.001149 sec.
Parallel quicksort took: 0.027126 sec.
Built-in quicksort took: 0.001301 sec.
Size: 10000
Sequential quicksort took: 0.001142 sec.
Parallel quicksort took: 0.027187 sec.
Built-in quicksort took: 0.001305 sec.
Size: 10000
Sequential quicksort took: 0.001149 sec.
Parallel quicksort took: 0.028343 sec.
Built-in quicksort took: 0.001302 sec.
Size: 100000
Sequential quicksort took: 0.014449 sec.
Parallel quicksort took: 0.040539 sec.
Built-in quicksort took: 0.016006 sec.
Size: 100000
Sequential quicksort took: 0.014423 sec.
Parallel quicksort took: 0.039298 sec.
Built-in quicksort took: 0.016042 sec.
Size: 100000
Sequential quicksort took: 0.014354 sec.
Parallel quicksort took: 0.041675 sec.
Built-in quicksort took: 0.016303 sec.
Size: 100000
Sequential quicksort took: 0.014382 sec.
Parallel quicksort took: 0.041028 sec.
Built-in quicksort took: 0.015984 sec.
Size: 100000
Sequential quicksort took: 0.014341 sec.
Parallel quicksort took: 0.038219 sec.
Built-in quicksort took: 0.015903 sec.
Size: 200000
Sequential quicksort took: 0.030129 sec.
Parallel quicksort took: 0.039334 sec.
Built-in quicksort took: 0.033760 sec.
Size: 200000
Sequential quicksort took: 0.030076 sec.
Parallel quicksort took: 0.036278 sec.
Built-in quicksort took: 0.033777 sec.
Size: 200000
Sequential quicksort took: 0.030209 sec.
Parallel quicksort took: 0.037004 sec.
Built-in quicksort took: 0.034514 sec.
Size: 200000
Sequential quicksort took: 0.030709 sec.
Parallel quicksort took: 0.038952 sec.
Built-in quicksort took: 0.033931 sec.
Size: 200000
Sequential quicksort took: 0.030256 sec.
Parallel quicksort took: 0.037556 sec.
Built-in quicksort took: 0.033847 sec.
Size: 300000
Sequential quicksort took: 0.046644 sec.
Parallel quicksort took: 0.048570 sec.
Built-in quicksort took: 0.051809 sec.
Size: 300000
Sequential quicksort took: 0.046852 sec.
Parallel quicksort took: 0.043541 sec.
Built-in quicksort took: 0.052050 sec.
Size: 300000
Sequential quicksort took: 0.046918 sec.
Parallel quicksort took: 0.047358 sec.
Built-in quicksort took: 0.054899 sec.
Size: 300000
Sequential quicksort took: 0.050399 sec.
Parallel quicksort took: 0.046212 sec.
Built-in quicksort took: 0.051833 sec.
Size: 300000
Sequential quicksort took: 0.046631 sec.
Parallel quicksort took: 0.042486 sec.
Built-in quicksort took: 0.051823 sec.
Size: 400000
Sequential quicksort took: 0.064408 sec.
Parallel quicksort took: 0.046936 sec.
Built-in quicksort took: 0.069743 sec.
Size: 400000
Sequential quicksort took: 0.063572 sec.
Parallel quicksort took: 0.048994 sec.
Built-in quicksort took: 0.069983 sec.
Size: 400000
Sequential quicksort took: 0.063211 sec.
Parallel quicksort took: 0.047949 sec.
Built-in quicksort took: 0.069953 sec.
Size: 400000
Sequential quicksort took: 0.063195 sec.
Parallel quicksort took: 0.046658 sec.
Built-in quicksort took: 0.070894 sec.
Size: 400000
Sequential quicksort took: 0.064935 sec.
Parallel quicksort took: 0.046050 sec.
Built-in quicksort took: 0.070098 sec.
Size: 500000
Sequential quicksort took: 0.080102 sec.
Parallel quicksort took: 0.049621 sec.
Built-in quicksort took: 0.089405 sec.
Size: 500000
Sequential quicksort took: 0.076246 sec.
Parallel quicksort took: 0.049176 sec.
Built-in quicksort took: 0.088076 sec.
Size: 500000
Sequential quicksort took: 0.077811 sec.
Parallel quicksort took: 0.050938 sec.
Built-in quicksort took: 0.089877 sec.
Size: 500000
Sequential quicksort took: 0.076792 sec.
Parallel quicksort took: 0.045259 sec.
Built-in quicksort took: 0.090210 sec.
Size: 500000
Sequential quicksort took: 0.077267 sec.
Parallel quicksort took: 0.048206 sec.
Built-in quicksort took: 0.090291 sec.
Size: 600000
Sequential quicksort took: 0.096853 sec.
Parallel quicksort took: 0.050421 sec.
Built-in quicksort took: 0.109393 sec.
Size: 600000
Sequential quicksort took: 0.095656 sec.
Parallel quicksort took: 0.059166 sec.
Built-in quicksort took: 0.109845 sec.
Size: 600000
Sequential quicksort took: 0.097657 sec.
Parallel quicksort took: 0.055835 sec.
Built-in quicksort took: 0.108208 sec.
Size: 600000
Sequential quicksort took: 0.098399 sec.
Parallel quicksort took: 0.054761 sec.
Built-in quicksort took: 0.109136 sec.
Size: 600000
Sequential quicksort took: 0.095845 sec.
Parallel quicksort took: 0.052791 sec.
Built-in quicksort took: 0.109019 sec.
Size: 700000
Sequential quicksort took: 0.109539 sec.
Parallel quicksort took: 0.068652 sec.
Built-in quicksort took: 0.129418 sec.
Size: 700000
Sequential quicksort took: 0.108108 sec.
Parallel quicksort took: 0.050006 sec.
Built-in quicksort took: 0.130167 sec.
Size: 700000
Sequential quicksort took: 0.109188 sec.
Parallel quicksort took: 0.051783 sec.
Built-in quicksort took: 0.129320 sec.
Size: 700000
Sequential quicksort took: 0.112425 sec.
Parallel quicksort took: 0.057089 sec.
Built-in quicksort took: 0.127907 sec.
Size: 700000
Sequential quicksort took: 0.111937 sec.
Parallel quicksort took: 0.053117 sec.
Built-in quicksort took: 0.129190 sec.
Size: 800000
Sequential quicksort took: 0.131754 sec.
Parallel quicksort took: 0.061653 sec.
Built-in quicksort took: 0.147248 sec.
Size: 800000
Sequential quicksort took: 0.134604 sec.
Parallel quicksort took: 0.055802 sec.
Built-in quicksort took: 0.148410 sec.
Size: 800000
Sequential quicksort took: 0.129597 sec.
Parallel quicksort took: 0.054604 sec.
Built-in quicksort took: 0.147145 sec.
Size: 800000
Sequential quicksort took: 0.131269 sec.
Parallel quicksort took: 0.050879 sec.
Built-in quicksort took: 0.148579 sec.
Size: 800000
Sequential quicksort took: 0.127987 sec.
Parallel quicksort took: 0.052845 sec.
Built-in quicksort took: 0.146346 sec.
Size: 900000
Sequential quicksort took: 0.146035 sec.
Parallel quicksort took: 0.056964 sec.
Built-in quicksort took: 0.168103 sec.
Size: 900000
Sequential quicksort took: 0.145996 sec.
Parallel quicksort took: 0.070508 sec.
Built-in quicksort took: 0.168595 sec.
Size: 900000
Sequential quicksort took: 0.146791 sec.
Parallel quicksort took: 0.057974 sec.
Built-in quicksort took: 0.168922 sec.
Size: 900000
Sequential quicksort took: 0.148079 sec.
Parallel quicksort took: 0.057358 sec.
Built-in quicksort took: 0.168602 sec.
Size: 900000
Sequential quicksort took: 0.146808 sec.
Parallel quicksort took: 0.053845 sec.
Built-in quicksort took: 0.169503 sec.
Size: 1000000
Sequential quicksort took: 0.164398 sec.
Parallel quicksort took: 0.071722 sec.
Built-in quicksort took: 0.187944 sec.
Size: 1000000
Sequential quicksort took: 0.163454 sec.
Parallel quicksort took: 0.067936 sec.
Built-in quicksort took: 0.187636 sec.
Size: 1000000
Sequential quicksort took: 0.164367 sec.
Parallel quicksort took: 0.061927 sec.
Built-in quicksort took: 0.186787 sec.
Size: 1000000
Sequential quicksort took: 0.164861 sec.
Parallel quicksort took: 0.058415 sec.
Built-in quicksort took: 0.188034 sec.
Size: 1000000
Sequential quicksort took: 0.168067 sec.
Parallel quicksort took: 0.053867 sec.
Built-in quicksort took: 0.187129 sec.
